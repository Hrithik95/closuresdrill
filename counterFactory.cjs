function counterFactory() {
    let count = 0;
    const counter = {
        increment: function () {
            count = count + 1;
            return count;
        },
        decrement: function () {
            count = count - 1;
            return count;
        }

    }
    return counter;
}

module.exports = counterFactory;

