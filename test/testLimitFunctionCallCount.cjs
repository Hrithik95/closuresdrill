const testLimitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function sayHello(...args) {
    console.log(args);
    return "hello";
}

try {
    const result = testLimitFunctionCallCount(sayHello, 1);
    console.log(result());
    console.log(result());
    console.log(result());
} catch (err) {
    console.log(err);
}