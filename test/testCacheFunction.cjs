const testCacheFunction = require('../cacheFunction.cjs');

function multiply(num) {
    return num * 2;
}
try {
    let result = testCacheFunction(multiply);
    console.log(result(1));
} catch (err) {
    console.error(err);
}