// function cacheFunction(cb) {
//     // Should return a function that invokes `cb`.
//     // A cache (object) should be kept in closure scope.
//     // The cache should keep track of all arguments have been used to invoke this function.
//     // If the returned function is invoked with arguments that it has already seen
//     // then it should return the cached result and not invoke `cb` again.
//     // `cb` should only ever be invoked once for a given set of arguments.
// }

// • Your cacheFunction implementation did not throw an error when there were no or partial parameters passed to it. Instead of returning a random data type, you can throw an error when the parameters fail a data check. You will then have to use try catch inside your test file to verify that the function is throwing the correct error message.
// • Your cacheFunction implementation did not invoke the callback correctly when the returned function was called. It should invoke it with all the parameters that were passed to the returned function. There is no fixed number of parameters that the callback function will take.
// • Your cacheFunction implementation did not return the correct value when the returned function was called. Make sure that you read and understand the problem before starting to write the code.
// • Your cacheFunction implementation did not invoke the callback when the returned function was called. Make sure that you read and understand the problem before starting to write the code.


function cacheFunction(callback) {
    if (typeof callback !== "function") {
        throw new Error("Error in passing argument in cacheFunction");
    }
    let cache = {};
    function execute(...args) {
        const key = JSON.stringify(args);
        if (key in cache) {
            return cache[key];
        } else {
            const result = callback(...args);
            cache[key] = result;
            return result;
        }

    }
    return execute;
}


module.exports = cacheFunction;
