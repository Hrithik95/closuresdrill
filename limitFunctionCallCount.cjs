// Your limitFunctionCallCount implementation did not throw an error when there were no or partial parameters passed to it. Instead of returning a random data type, you can throw an error when the parameters fail a data check. You will then have to use try catch inside your test file to verify that the function is throwing the correct error message.
// Your limitFunctionCallCount implementation did not invoke the callback correctly when the returned function was called. It should invoke it with all the parameters that were passed to the returned function. There is no fixed number of parameters that the callback function will take.

function limitFunctionCallCount(callbackToExecute, maxNumberToBeExecuted) {
    if (typeof callbackToExecute !== "function" || typeof maxNumberToBeExecuted !== "number" || maxNumberToBeExecuted < 0) {
        throw new Error("Error in passing the parameters");
    }
    let numberOfExecutions = 0;
    function execute(...args) {
        numberOfExecutions++;
        if (numberOfExecutions <= maxNumberToBeExecuted) {
            return callbackToExecute(...args);
        } else {
            return null;
        }
    }
    return execute;
}




module.exports = limitFunctionCallCount;
